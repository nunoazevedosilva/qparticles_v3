# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 11:04:20 2017

Routine to make some benchmark graphs

@author: nunoazevedosilva
"""

from scipy import *
from pylab import *

def read_optimization_file(filename):
    """
    Function to read from filename the optimization results
    """
    text = open(filename)
    text.readline()
    Nparticles = int(text.readline())
    text.readline()
    line = text.readline()
    lista2 = []
    while(line!=""):
        lista = line.split("\t")
        lista[1]=lista[1].split("\n")[0]
        lista2.append(array([int(lista[0]),float(lista[1])]))
        line = text.readline()
        
    return filename,lista2, Nparticles
    

labels3=[]
benchs3=[]
f="Ncells_optimization.dat"
f1,l1,Nparticles = read_optimization_file(f)
labels3.append(Nparticles)
benchs3.append(array(l1))


xxs=benchs3[0][:,0]
benchs3[0][:,1]=benchs3[0][:,1]/1000.

xmin2=benchs3[0][0,0]
xmax=benchs3[0][-1,0]
ymin=min(benchs3[0][:,1])

figure(1,[8,4])
plot(xxs,benchs3[0][:,1],'o',markersize=8,fillstyle='none',ls='-',color='k', label = "$N_{p}=$"+str(Nparticles))


xmin=xxs[0]
xmax=xxs[-1]
factorx=(xmax-xmin)*0.1
ymax=max(benchs3[0][:,1])
factory=(ymax-ymin)*0.1
xlim([xmin2,xmax+factorx])
ylim([ymin-factory,ymax+factory])


x_list_ticks=[xmin,xmax]
y_list_ticks=[ymin,ymax]


highlight_xy=[[float(xxs[where(benchs3[0][:,1]==ymin)]),ymin]]
highlight_y=[]
highlight_x=[]
#highlight_xy=[[90000,max3],[benchs3[0][1,0],speedups[1]]]


for x_h0 in highlight_x:
    x_h=x_h0[0]
    y_h=x_h0[1]
    plot([x_h,x_h],[ymin-factory,y_h],color="k",ls=':',alpha=0.4)

    x_list_ticks.append(x_h)


for y_h0 in highlight_y:
    x_h=y_h0[0]
    y_h=y_h0[1]
    plot([x_h,x_h],[ymin-factory,y_h],color="k",ls=':',alpha=0.4)
    plot([xmin2,x_h],[y_h,y_h],color="k",ls=':',alpha=0.4)
    x_list_ticks.append(x_h)
    
for xy_h in highlight_xy:
    x_h=xy_h[0]
    y_h=xy_h[1]
    plot([x_h,x_h],[ymin-factory,y_h],color="k",ls=':',alpha=0.4)
    plot([xmin2,x_h],[y_h,y_h],color="k",ls=':',alpha=0.4)
    x_list_ticks.append(x_h)
    y_list_ticks.append(y_h)
    
#xticks([0,0.15,0.3],size=18)
x_str=[]
for i in range(0,len(x_list_ticks)):
    x_str.append(str(2*int(log2(x_list_ticks[i]))))
    

xticks(x_list_ticks,size=18)
yticks(y_list_ticks,size=18)

ax=gca()
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_bounds(xmin,xmax)
ax.xaxis.set_ticks_position('bottom')
ax.spines['left'].set_bounds(ymin, ymax)
ax.spines["right"].set_visible(False)
ax.yaxis.set_ticks_position('left')

ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

ylabel(r"$t(s)$",size=22,rotation=0)
xlabel(r"$\sqrt{N_{cells}}$",size=22)

legend(loc=5)

savefig("OptimizeN", bbox_inches="tight",dpi=300) 

        
    
    
