#pragma once

/************************************************
 *  QPARTICLES:
 * a code to simulate many-body quantum particles 
 * in
 * interaction with electromagnetic fields
 * in massively parallel HPC environments
 *
 * author: Nuno Azevedo Silva 
 * nuno.a.silva@inesctec.pt
 ***********************************************/

/* Cell.h : class for creating a cell, containing particles */

#include "particle.h"

/// Class for a single cell in a cell system
/** Class for keeping track the contents of a single Cell */
class Cell {

public:

	vector<Particle> particles;		//!< vector containing the particles

	double Cell_lx;					//!< cell dimension - x direction
	double Cell_ly;					//!< cell dimension - y direction
	double Cell_lz;					//!< cell dimension - z direction

	int x_pos_abs;					//!< position in absolute map of cells
	int y_pos_abs;					//!< position in absolute map for cells
	int z_pos_abs;

	vector<vector<double>> remote_particles_middle_plane;	//!<  Buffer to store the particle data of neighboors
	vector<vector<double>> remote_particles_top_plane;		//!<  Buffer to store the particle data of neighboors
	vector<vector<double>> remote_particles_bottom_plane;	//!<  Buffer to store the particle data of neighboors
 
	vector<double> PartBuff_mp;			//!<  Buffer to store the particle data to send
	vector<double> PartBuff_tp;			//!<  Buffer to store the particle data to send
	vector<double> PartBuff_bp;			//!<  Buffer to store the particle data to send

	MPI_Request reqr_middle_plane[8], reqs_middle_plane[8]; //!<  requests, middle plane
	int nreqr_middle_plane, nreqs_middle_plane;				//!<  #requests, middle plane

	MPI_Request reqr_top_plane[9], reqs_top_plane[9];		//!<  requests, top plane
	int nreqr_top_plane, nreqs_top_plane;					//!<  requests, top plane

	MPI_Request reqr_bottom_plane[9], reqs_bottom_plane[9];	//!<  requests, bottom plane
	int nreqr_bottom_plane, nreqs_bottom_plane;				//!<  requests, bottom plane

	int nparticles;				//!<  number of particles in current cell

	int rank_cell;					//!< rank of the cpu that owns the cell


	vector<vector<cPrecision>> particles_to_send;	//!<  Buffer to store the particles to send to neighboors
	vector<vector<double>> particles_to_receive;	//!<  Buffer to store the particles to receive from neighboors




	//! Constructs a cell in position i,j,k at a given node and system
	Cell(int i, int j, int k, int Node_x, int Node_y, int Node_z, int Nx_per_node, int Ny_per_node, int Nz_per_node, int nParticles, double Cell_lx, double Cell_ly, double Cell_lz,int nlevels = NLEVELS) :
		particles(nParticles,Particle(NLEVELS)), 
		Cell_lx(Cell_lx), Cell_ly(Cell_ly), Cell_lz(Cell_lz), nparticles(nParticles),
		remote_particles_middle_plane(vector<vector<double>>(8, vector<double>(nParticles * 3 * 4 + 1, 0))),
		remote_particles_top_plane(vector<vector<double>>(9, vector<double>(nParticles * 3 * 4 + 1, 0))),
		remote_particles_bottom_plane(vector<vector<double>>(9, vector<double>(nParticles * 3 * 4 + 1, 0))),
		PartBuff_mp(vector<double>(nParticles * 3 * 4 + 1,0))


	{

		MPI_Comm_rank(MPI_COMM_WORLD, &rank_cell);
		//compute the absolute position of the cell in the system overall
		x_pos_abs = Node_x*Nx_per_node + i;
		y_pos_abs = Node_y*Ny_per_node + j;
		z_pos_abs = Node_z*Nz_per_node + k;
	

		//  Filling the cell		
		double boundx = Cell_lx * double(i) + Cell_lx * Nx_per_node* Node_x;
		double boundy = Cell_ly * double(j) + Cell_ly * Ny_per_node* Node_y;
		double boundz = Cell_lz * double(k) + Cell_lz * Nz_per_node* Node_z;
		
		

		double dx = Cell_lx;
		double dy = Cell_ly;
		double dz = Cell_lz;

		//distribute the particles randomly, with random velocity for now but should be updated to a velocity distribution
		for (int p = 0; p < particles.size(); p++)
		{
			particles[p] = Particle(NLEVELS);
			particles[p].x = boundx + ((float(rand()) / RAND_MAX)*(dx));
			particles[p].y = boundy + ((float(rand()) / RAND_MAX)*(dy));
			particles[p].z = boundz + ((float(rand()) / RAND_MAX)*(dz));
			particles[p].vx = ((float(rand()) / RAND_MAX - 0.5))*3.;
			particles[p].vy = ((float(rand()) / RAND_MAX - 0.5))*3.;
			particles[p].vz = ((float(rand()) / RAND_MAX - 0.5))*3.;
		}

		// Allocate the necessary space for particles * 4 for now, need to think of a strategy for this later
		

	}

	//! Packs the message to send to neighboors
	void pack_message()
	{

		//send in the first position the size of the message
		PartBuff_mp[0] = particles.size();
		//and each degree of freedom you want to send (for now only sending the positions, but in the future will need density operator)
		for (int i = 0; i < particles.size(); i++)
		{
			PartBuff_mp[1 + i * 3] = particles[i].x;
			PartBuff_mp[1 + i * 3 + 1] = particles[i].y;
			PartBuff_mp[1 + i * 3 + 2] = particles[i].z;
		}

	}

	//! Communicate the middle plane cells and receive them
	void Communicate_middle_plane(map CellMap, int Dimensionx, int Dimensiony, int Dimensionz) {

		// Receive calls first
		
		//a counter
		int ircount = 0;
		
		// i+1,j,k
		if (x_pos_abs != Dimensionx - 1)
		{   
			int tag = CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs][0];
			if (rank_cell != rank_comm){
				MPI_Irecv(&remote_particles_middle_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
				ircount++;
			}
		}
		
		//i-1,j,k
		if (x_pos_abs != 0) 
		{
			int tag = CellMap[x_pos_abs-1][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Irecv(&remote_particles_middle_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
				ircount++;
			}
		}

		
		// i,j+1,k
		if (y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Irecv(&remote_particles_middle_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
				ircount++;
			}
		}
		// i,j-1,k
		if (y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Irecv(&remote_particles_middle_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
				ircount++;
			}
		}
		
		// i+1,j+1,k
		if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs+1][y_pos_abs+1][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs+1][y_pos_abs+1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Irecv(&remote_particles_middle_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
				ircount++;
			}
		}
		//i+1,j-1,k
		if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Irecv(&remote_particles_middle_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
				ircount++;
			}
		}
		//i-1,j+1,k
		if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs - 1][y_pos_abs+1][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs - 1][y_pos_abs+1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Irecv(&remote_particles_middle_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
				ircount++;
			}
		}
		//i-1,j-1,k
		if (x_pos_abs != 0 && y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs - 1][y_pos_abs-1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Irecv(&remote_particles_middle_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_middle_plane[ircount]);
				ircount++;
			}
		}
		/**/

		
		
		// Now the sending
		int iscount = 0;

		
		//i+1,j,k
		if (x_pos_abs != Dimensionx - 1) 
		{
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs+1][y_pos_abs][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
				iscount++;
			}
			
		}
		
		// i-1,j,k
		if (x_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
				iscount++;
			}
		}

		
		
		// i,j+1,k
		if (y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
				iscount++;
			}
		}
		
		// i,j-1,k
		if (y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
				iscount++;
			}
		}
		
		
		// i+1,j+1,k
		if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs+1][y_pos_abs+1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
				iscount++;
			}
		}
		//i+1,j-1,k
		if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs+1][y_pos_abs-1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
				iscount++;
			}
		}
		//i-1,j+1,k
		if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs-1][y_pos_abs+1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
				iscount++;
			}
		}
		//i-1,j-1,k
		if (x_pos_abs != 0 && y_pos_abs != 0) {
			int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
			int rank_comm = CellMap[x_pos_abs-1][y_pos_abs-1][z_pos_abs][0];
			if (rank_cell != rank_comm) {
				MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_middle_plane[iscount]);
				iscount++;
			}
		}

		nreqr_middle_plane = ircount;
		nreqs_middle_plane = iscount;

	}

	//! communicate the top plane cells
	void Communicate_top_plane(map CellMap, int Dimensionx, int Dimensiony, int Dimensionz) {


		int ircount = 0;
		int iscount = 0;
		
		//make sure there is space above it, otherwise do not send it
		if (z_pos_abs != Dimensionz - 1)
		{
			
			// Receive calls
			// i+1,j,k+1
			
			if (x_pos_abs != Dimensionx - 1)
			{  
				int tag = CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}
			
			//i-1,j,k+1
			if (x_pos_abs != 0) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}
			// i,j+1,k+1
			if (y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs + 1][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}
			// i,j-1,k+1
			if (y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}
			// i+1,j+1,k+1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs + 1][y_pos_abs + 1][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs+1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}
			//i+1,j-1,k+1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs-1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}
			//i-1,j+1,k+1
			if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs+1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}
			//i-1,j-1,k+1
			if (x_pos_abs != 0 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs-1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}
			//i,j,k+1
			if (true) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs + 1][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_top_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_top_plane[ircount]);
					ircount++;
				}
			}

			// Now the sending
			//i+1,j,k+1
			if (x_pos_abs != Dimensionx - 1)
			{
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}
			// i-1,j,k+1
			if (x_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}
			// i,j+1,k+1
			if (y_pos_abs != Dimensiony-1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}
			// i,j-1,k+1
			if (y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}
			// i+1,j+1,k+1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs+1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}
			//i+1,j-1,k+1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs-1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}
			//i-1,j+1,k+1
			if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs+1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}
			//i-1,j-1,k+1
			if (x_pos_abs != 0 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs-1][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}

			//i,j,k+1
			if (true) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs][z_pos_abs+1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_top_plane[iscount]);
					iscount++;
				}
			}
			
		}

		nreqr_top_plane = ircount;
		nreqs_top_plane = iscount;

	}

	//! communicate the bottom plane cells
	void Communicate_bottom_plane(map CellMap, int Dimensionx, int Dimensiony, int Dimensionz) {


		int ircount = 0;
		int iscount = 0;

		//make sure there is space below..
		if (z_pos_abs != 0)
		{
			// Receive calls
			// i+1,j,k-1
			if (x_pos_abs != Dimensionx - 1)
			{   
				int tag = CellMap[x_pos_abs + 1][y_pos_abs][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs][z_pos_abs-1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}
			//i-1,j,k-1
			if (x_pos_abs != 0) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs - 1][y_pos_abs][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}
			// i,j+1,k-1
			if (y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs + 1][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}
			// i,j-1,k-1
			if (y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs - 1][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}
			// i+1,j+1,k-1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs + 1][y_pos_abs + 1][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs+1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}
			//i+1,j-1,k-1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs + 1][y_pos_abs - 1][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs-1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}
			//i-1,j+1,k-1
			if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs + 1][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs+1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}
			//i-1,j-1,k-1
			if (x_pos_abs != 0 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs - 1][y_pos_abs - 1][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs-1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}
			//i,j,k-1
			if (true) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs - 1][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Irecv(&remote_particles_bottom_plane[ircount][0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqr_bottom_plane[ircount]);
					ircount++;
				}
			}

			// Now the sending

			//i+1,j,k-1
			if (x_pos_abs != Dimensionx - 1)
			{
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}
			// i-1,j,k-1
			if (x_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}
			// i,j+1,k-1
			if (y_pos_abs != Dimensiony -1 ) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs+1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}
			// i,j-1,k-1
			if (y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs-1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}
			// i+1,j+1,k-1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs+1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}
			//i+1,j-1,k-1
			if (x_pos_abs != Dimensionx - 1 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs+1][y_pos_abs-1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}
			//i-1,j+1,k-1
			if (x_pos_abs != 0 && y_pos_abs != Dimensiony - 1) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs+1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}
			//i-1,j-1,k-1
			if (x_pos_abs != 0 && y_pos_abs != 0) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs-1][y_pos_abs-1][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}

			//i,j,k-1
			if (true) {
				int tag = CellMap[x_pos_abs][y_pos_abs][z_pos_abs][1];
				int rank_comm = CellMap[x_pos_abs][y_pos_abs][z_pos_abs - 1][0];
				if (rank_cell != rank_comm) {
					MPI_Isend(&PartBuff_mp[0], nparticles * 12, MPI_DOUBLE, rank_comm, tag, MPI_COMM_WORLD, &reqs_bottom_plane[iscount]);
					iscount++;
				}
			}
		}

		nreqr_bottom_plane = ircount;
		nreqs_bottom_plane = iscount;

	}

	//!wait for middle plane comms
	void PostWaits_middle_plane(void) {
		MPI_Status statr[8];
		MPI_Status stats[8];
		MPI_Waitall(nreqs_middle_plane, reqs_middle_plane, stats);
		MPI_Waitall(nreqr_middle_plane, reqr_middle_plane, statr);
	}

	//!wait for top plane comms
	void PostWaits_top_plane(void) {
		MPI_Status statr[9];
		MPI_Status stats[9];
		MPI_Waitall(nreqs_top_plane, reqs_top_plane, stats);
		MPI_Waitall(nreqr_top_plane, reqr_top_plane, statr);
	}

	//!wait for bottom plane comms
	void PostWaits_bottom_plane(void) {
		MPI_Status statr[9];
		MPI_Status stats[9];
		MPI_Waitall(nreqs_bottom_plane, reqs_bottom_plane, stats);
		MPI_Waitall(nreqr_bottom_plane, reqr_bottom_plane, statr);

	}

};

