#include "Cell_system_node.h"
#include "tests_scaling.h"
#include "tacc_sim.h"
#include "dipole_force_simulations.h"


int main(int argc, char* argv[])
{

	//MPI_Init(&argc, &argv);
	int required = MPI_THREAD_MULTIPLE;
	int provided = 0;
	MPI_Init_thread(&argc, &argv, required, &provided);
	int nodes_x;
	MPI_Comm_size(MPI_COMM_WORLD, &nodes_x);

	
	//strong scaling pure mpi
	//int b = strong_scaling3("benchs/", nodes_x);

	//strong scaling hybrid
	//int b = strong_scaling_thread3("benchs/", nodes_x);

	// light diffusion simulation
	//int a = tacc_sim(argc,argv);

	//dipole force simulation
	int a = dipole_force_simulation(argc, argv);

	MPI_Finalize();
	return 0;
}

/**/
