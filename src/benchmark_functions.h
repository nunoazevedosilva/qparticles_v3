#pragma once

#include "Cell_system_node.h"

double envelope_neg(double x, double y, double z, double t)
{
  ////////////////////////////////////////////////

  // Dimension of the total system in micrometers
  double Lx = 2.0 * 10e2;
  double Ly = 2.0 * 10e2;
  double Lz = 1.0 * 10e1;

  // Setting the EMF fields
  double x_0 = Lx / 4;
  double y_0 = Ly / 4;
  double z_0 = Lz / 4;

  double x_1 = 3 * Lx / 4;
  double y_1 = 3 * Ly / 4;
  double z_1 = 3 * Lz / 4;

  double x_2 = 3 * Lx / 4;
  double y_2 = 1 * Ly / 4;
  double z_2 = 3 * Lz / 4;

  double wx = Lx / 8.;
  double wy = Ly / 8.;
  double wz = Lz / 8.;

  ////////////////////////////////////////////////

  return exp(-(pow((x - x_0) / wx, 2) + pow((y - y_0) / wy, 2) +
               0 * pow((z - z_0) / wz, 2)));
}

double envelope_pos(double x, double y, double z, double t)
{
  ////////////////////////////////////////////////

  // Dimension of the total system in micrometers
  double Lx = 2.0 * 10e2;
  double Ly = 2.0 * 10e2;
  double Lz = 1.0 * 10e1;

  // Setting the EMF fields
  double x_0 = Lx / 4;
  double y_0 = Ly / 4;
  double z_0 = Lz / 4;

  double x_1 = 3 * Lx / 4;
  double y_1 = 3 * Ly / 4;
  double z_1 = 3 * Lz / 4;

  double x_2 = 3 * Lx / 4;
  double y_2 = 1 * Ly / 4;
  double z_2 = 3 * Lz / 4;

  double wx = Lx / 8.;
  double wy = Ly / 8.;
  double wz = Lz / 8.;

  ////////////////////////////////////////////////

  return exp(-(pow((x - x_1) / wx, 2) + pow((y - y_1) / wy, 2) +
               0 * pow((z - z_1) / wz, 2)));
}

double envelope_double(double x, double y, double z, double t)
{
  ////////////////////////////////////////////////

  // Dimension of the total system in micrometers
  double Lx = 2.0 * 10e2;
  double Ly = 2.0 * 10e2;
  double Lz = 1.0 * 10e1;

  // Setting the EMF fields
  double x_0 = Lx / 4;
  double y_0 = Ly / 4;
  double z_0 = Lz / 4;

  double x_1 = 3 * Lx / 4;
  double y_1 = 3 * Ly / 4;
  double z_1 = 3 * Lz / 4;

  double x_2 = 3 * Lx / 4;
  double y_2 = 1 * Ly / 4;
  double z_2 = 3 * Lz / 4;

  double wx = Lx / 8.;
  double wy = Ly / 8.;
  double wz = Lz / 8.;

  ////////////////////////////////////////////////

  return exp(-(pow((x - x_2) / wx, 2) + pow((y - y_2) / wy, 2) +
               0 * pow((z - z_2) / wz, 2)));
}

int optimize_n(string saveDir, int Nmin, int Nmax, int Nstep)
{
  int n_threads = 8;

  // integration step
  double dt = 0.05;

  // density of the gas
  const double density = 10e-5;  // parts/micrometer**3

  // Dimension of the total system in micrometers
  double Lx = 2.0 * 10e2;
  double Ly = 2.0 * 10e2;
  double Lz = 1.0 * 10e1;

  // set a mesh for saving the EMF field
  int Nx = 50;
  int Ny = 50;
  int Nz = 50;

  // create a mesh with simulation parameters
  mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

  //////////////////////////////////////////////////
  // initialize the need electric fields
  ElectricField E_p1 = ElectricField(0.5, -0.01, "E_p1", envelope_pos);
  ElectricField E_p2 = ElectricField(0.5, 0.01, "E_p2", envelope_neg);

  vector<ElectricField> EMFs;
  EMFs.push_back(E_p1);
  EMFs.push_back(E_p2);

  ///////////////////////////////////////////////

  // parameters for the simulation - machine related
  int nodes_X = 1;
  int nodes_Y = 1;
  int nodes_Z = 1;

  int NumParticles = density * Lx * Ly * Lz;

  std::ofstream param(
      saveDir + std::string("/Ncells_optimization" + std::string(".dat")));
  param << "Nparticles\n";
  param << NumParticles;
  param << "\n";

  param << "Points\t";
  param << "Time elapsed\n";

  // for each nn in range 5-Nmax
  for (int nn = Nmin; nn < Nmax; nn += Nstep) {
    int Ncells_X_node = nn;
    int Ncells_Y_node = nn;
    int Ncells_Z_node = 1;

    int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node;

    // dimensions of the cell in micrometers
    double Cell_lx = Lx / nodes_X / Ncells_X_node;
    double Cell_ly = Ly / nodes_Y / Ncells_Y_node;
    double Cell_lz = Lz / nodes_Z / Ncells_Z_node;

    // number of particles
    const int NPartPerCell = int(density * Cell_lx * Cell_ly * Cell_lz);

    // create the cell system in 1 node
    Cell_system_node total_cells = Cell_system_node(Ncells_X_node,
                                                    Ncells_Y_node,
                                                    Ncells_Z_node,
                                                    Cell_lx,
                                                    Cell_ly,
                                                    Cell_lz,
                                                    dt,
                                                    my_mesh,
                                                    EMFs,
                                                    NPartPerCell,
                                                    NLEVELS,
                                                    true,
                                                    n_threads);

    cout << "************************\nThe Total Number of Cells is "
         << TotalCells << "\nand " << NumParticles
         << " particles total in system"
         << "\nand " << NPartPerCell << " particles in each cell \n";

    cout << "Cells done\n";

    // start a timer
    auto start = std::chrono::system_clock::now();

    // run the simulation
    int NumIterations = 50;
    total_cells.run(NumIterations, 100, dt, "data2\\", "short_range", false);

    // end the timer
    auto end = std::chrono::system_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    cout << " Number of Cells " << TotalCells;
    cout << endl << "time taken = " << elapsed.count() << "ms" << '\n';

    param << nn << "\t" << elapsed.count() << "\n";
  }

  param.close();
  printf("Finalized test, enter to exit");
  int a = getchar();
  return 0;
}

int bench_npart_shortrange(string saveDir,
                           float Nmin,
                           float Nmax,
                           float Nstep,
                           int Ncell,
                           int n_threads)
{
  // Dimension of the total system in micrometers
  double Lx = 2.0 * 10e2;
  double Ly = 2.0 * 10e2;
  double Lz = 1.0 * 10e1;

  std::ofstream param(saveDir +
                      std::string("/TimevsNpart" + std::string(".dat")));

  param << "Ncells\n";
  param << Ncell;
  param << "\n";

  param << "Nparticles\t";
  param << "Time elapsed\n";

  // integration step
  double dt = 0.05;

  // for each nn in range 5-Nmax
  for (float nn = Nmin; nn < Nmax; nn += Nstep) {
    // density of the gas
    const double density = nn * 10e-5;  // parts/micrometer**3

    // set a mesh for saving the EMF field
    int Nx = 50;
    int Ny = 50;
    int Nz = 50;

    // create a mesh with simulation parameters
    mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

    //////////////////////////////////////////////////
    // initialize the need electric fields
    ElectricField E_p1 = ElectricField(0.5, -0.01, "E_p1", envelope_pos);
    ElectricField E_p2 = ElectricField(0.5, 0.01, "E_p2", envelope_neg);

    vector<ElectricField> EMFs;
    EMFs.push_back(E_p1);
    EMFs.push_back(E_p2);

    ///////////////////////////////////////////////

    // parameters for the simulation - machine related
    int nodes_X = 1;
    int nodes_Y = 1;
    int nodes_Z = 1;

    int NumParticles = density * Lx * Ly * Lz;

    int Ncells_X_node = Ncell;
    int Ncells_Y_node = Ncell;
    int Ncells_Z_node = 1;

    int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node;

    // dimensions of the cell in micrometers
    double Cell_lx = Lx / nodes_X / Ncells_X_node;
    double Cell_ly = Ly / nodes_Y / Ncells_Y_node;
    double Cell_lz = Lz / nodes_Z / Ncells_Z_node;

    // number of particles
    const int NPartPerCell = int(density * Cell_lx * Cell_ly * Cell_lz);

    // create the cell system in 1 node
    Cell_system_node total_cells = Cell_system_node(Ncells_X_node,
                                                    Ncells_Y_node,
                                                    Ncells_Z_node,
                                                    Cell_lx,
                                                    Cell_ly,
                                                    Cell_lz,
                                                    dt,
                                                    my_mesh,
                                                    EMFs,
                                                    NPartPerCell,
                                                    NLEVELS,
                                                    true,
                                                    n_threads);

    cout << "************************\nThe Total Number of Cells is "
         << TotalCells << "\nand " << NumParticles
         << " particles total in system"
         << "\nand " << NPartPerCell << " particles in each cell \n";

    cout << "Cells done\n";

    // start a timer
    auto start = std::chrono::system_clock::now();

    // run the simulation
    int NumIterations = 50;
    total_cells.run(NumIterations, 100, dt, "", "short_range", false);

    // end the timer
    auto end = std::chrono::system_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    cout << " Number of Cells " << TotalCells;
    cout << endl << "time taken = " << elapsed.count() << "ms" << '\n';

    param << NPartPerCell * Ncells_X_node * Ncells_Y_node * Ncells_Z_node
          << "\t" << elapsed.count() << "\n";
  }

  param.close();
  printf("Finalized test, enter to exit");
  int a = getchar();
  return 0;
}

int bench_threads(string saveDir, int Ncell, int n_threadsmax)
{
  // Dimension of the total system in micrometers
  double Lx = 2.0 * 10e2;
  double Ly = 2.0 * 10e2;
  double Lz = 1.0 * 10e1;

  std::ofstream param(saveDir +
                      std::string("/tivevsthreads" + std::string(".dat")));

  param << "Ncells\n";
  param << Ncell;
  param << "\n";

  param << "Nparticles\t";
  param << "Time elapsed\n";

  // integration step
  double dt = 0.05;

  // for each nn in range 5-Nmax
  for (float nthreads = 1; nthreads < n_threadsmax; nthreads += 1) {
    // density of the gas
    const double density = 10e-5;  // parts/micrometer**3

    // set a mesh for saving the EMF field
    int Nx = 50;
    int Ny = 50;
    int Nz = 50;

    // create a mesh with simulation parameters
    mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

    //////////////////////////////////////////////////
    // initialize the need electric fields
    ElectricField E_p1 = ElectricField(0.5, -0.01, "E_p1", envelope_pos);
    ElectricField E_p2 = ElectricField(0.5, 0.01, "E_p2", envelope_neg);

    vector<ElectricField> EMFs;
    EMFs.push_back(E_p1);
    EMFs.push_back(E_p2);

    ///////////////////////////////////////////////

    // parameters for the simulation - machine related
    int nodes_X = 1;
    int nodes_Y = 1;
    int nodes_Z = 1;

    int NumParticles = density * Lx * Ly * Lz;

    int Ncells_X_node = Ncell;
    int Ncells_Y_node = Ncell;
    int Ncells_Z_node = 1;

    int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node;

    // dimensions of the cell in micrometers
    double Cell_lx = Lx / nodes_X / Ncells_X_node;
    double Cell_ly = Ly / nodes_Y / Ncells_Y_node;
    double Cell_lz = Lz / nodes_Z / Ncells_Z_node;

    // number of particles
    const int NPartPerCell = int(density * Cell_lx * Cell_ly * Cell_lz);

    // create the cell system in 1 node
    Cell_system_node total_cells = Cell_system_node(Ncells_X_node,
                                                    Ncells_Y_node,
                                                    Ncells_Z_node,
                                                    Cell_lx,
                                                    Cell_ly,
                                                    Cell_lz,
                                                    dt,
                                                    my_mesh,
                                                    EMFs,
                                                    NPartPerCell,
                                                    NLEVELS,
                                                    true,
                                                    nthreads);

    cout << "************************\nThe Total Number of Cells is "
         << TotalCells << "\nand " << NumParticles
         << " particles total in system"
         << "\nand " << NPartPerCell << " particles in each cell \n";

    cout << "Cells done\n";

    // start a timer
    auto start = std::chrono::system_clock::now();

    // run the simulation
    int NumIterations = 500;
    total_cells.run(NumIterations, 100, dt, "", "short_range", false);

    // end the timer
    auto end = std::chrono::system_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    cout << " Nthreads " << nthreads;
    cout << endl << "time taken = " << elapsed.count() << "ms" << '\n';

    param << nthreads << "\t" << elapsed.count() << "\n";
  }

  param.close();
  printf("Finalized test, enter to exit");
  int a = getchar();
  return 0;
}