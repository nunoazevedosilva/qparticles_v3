#pragma once

/************************************************
 *  QPARTICLES:
 * a code to simulate many-body quantum particles
 * in
 * interaction with electromagnetic fields
 * in massively parallel HPC environments
 *
 * author: Nuno Azevedo Silva
 * nuno.a.silva@inesctec.pt
 ***********************************************/

/* definitions.h: contains the necessary external includes */

#include <mpi.h>
#include <omp.h>
#include <stdlib.h>
#include <vtkActor.h>
#include <vtkCellArray.h>
#include <vtkDataSetMapper.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkStructuredGrid.h>
#include <vtkTetra.h>
#include <vtkUnstructuredGrid.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkXMLPUnstructuredGridWriter.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <boost/filesystem.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <chrono>
#include <cmath>
#include <complex>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "simulation_parameters.h"

#define PI 3.14159265358979323846

using std::cin;
using std::cout;
using std::endl;
using std::minus;
using std::plus;
using std::string;
using std::to_string;
using std::transform;
using std::vector;

using namespace boost::numeric;

typedef std::complex<double> cPrecision;

typedef std::vector<std::vector<std::vector<std::vector<int>>>> map;
