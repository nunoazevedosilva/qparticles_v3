#pragma once
#include "Cell_system_node.h"

double envelope_probe(double x, double y, double z, double t)
{
	////////////////////////////////////////////////

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;


	//Setting the EMF fields
	double x_0 = Lx / 2;
	double y_0 = Ly / 2;
	double z_0 = Lz / 4;

	double wx = Lx / 4.;
	double wy = Ly / 4.;
	double wz = Lz / 4.;

	////////////////////////////////////////////////

	return exp(-(pow((x - x_0) / wx, 2) + pow((y - y_0) / wy, 2) + 0 * pow((z - z_0) / wz, 2)));
}

double envelope_control(double x, double y, double z, double t)
{
	////////////////////////////////////////////////

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 1.0*10e1;


	//Setting the EMF fields
	double x_0 = Lx / 2;
	double y_0 = Ly / 2;
	double z_0 = Lz / 4;

	double wx = Lx / 4.;
	double wy = Ly / 4.;
	double wz = Lz / 4.;

	////////////////////////////////////////////////

	return 1.0;
}

int dipole_force_simulation(int argc, char* argv[])
{
	

	//integration step
	double dt = 0.05;
	//Number of iterations
	int NumIterations = 8000;

	//density of the gas
	const double density = 10e-6; //parts/micrometer**3

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 2.0*10e2;
	double Lz = 5.0*10e1;

	//set a mesh for saving the EMF field
	int Nx = 50;
	int Ny = 50;
	int Nz = 50;

	//create a mesh with simulation parameters
	mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

	//////////////////////////////////////////////////
	//initialize the need electric fields
	ElectricField E_p1 = ElectricField(0.5, -0.01, "E_p1", envelope_probe);
	ElectricField E_c = ElectricField(0.0, 0.0, "E_p3", envelope_control);

	vector<ElectricField> EMFs;
	EMFs.push_back(E_p1);
	EMFs.push_back(E_c);

	///////////////////////////////////////////////

	//parameters for the simulation - machine related
	int TotalNodes_X = 1;
	int TotalNodes_Y = 1;
	int TotalNodes_Z = 1;

	int Ncells_X_node = int(10 / TotalNodes_X);
	int Ncells_Y_node = int(10 / TotalNodes_Y);
	int Ncells_Z_node = 5;

	int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node*TotalNodes_X*TotalNodes_Y*TotalNodes_Z;

	//dimensions of the cell in micrometers
	double Cell_lx = Lx / TotalNodes_X / Ncells_X_node;
	double Cell_ly = Ly / TotalNodes_Y / Ncells_Y_node;
	double Cell_lz = Lz / TotalNodes_Z / Ncells_Z_node;

	//number of particles
	const int NPartPerCell = int(density*Cell_lx*Cell_ly*Cell_lz);
	int NumParticles = density * Lx*Ly*Lz;

	//create the cell system in 1 node
	Cell_system_node total_cells = Cell_system_node(TotalNodes_X, TotalNodes_Y, TotalNodes_Z, Ncells_X_node, Ncells_Y_node, Ncells_Z_node, Cell_lx, Cell_ly, Cell_lz, dt, my_mesh, EMFs, NPartPerCell, NLEVELS, true);

	cout << "\nThe Total Number of Cells is " << TotalCells
		<< "\nand " << NumParticles << " particles total in system"
		<< "\nand " << NPartPerCell << " particles in each cell \n";

	cout << "Cells done\n";

	//start a timer
	auto start = std::chrono::system_clock::now();

	//run the simulation

	total_cells.run(NumIterations, 10, dt, "dipole_force_test//", "short_range");


	//end the timer
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	cout << endl << "time taken = " << elapsed.count() << "ms" << '\n';

	return 0;
}

