#pragma once
/************************************************
 *  QPARTICLES:
 * a code to simulate many-body quantum particles
 * in
 * interaction with electromagnetic fields
 * in massively parallel HPC environments
 *
 * author: Nuno Azevedo Silva
 * nuno.a.silva@inesctec.pt
 ***********************************************/

 /* particle.h include a class for creating each particle, with position, velocity, forces and density operator */

#include "density_operator.h"
#include <mutex>
//#include <memory>

//! Class to create a particle, with the necessary properties
class Particle {
//private:
//	std::shared_ptr<std::mutex> _m;

public:

	double x;		//!< position in x axis
	double y;		//!< position in y axis
	double z;		//!< position in z axis

	double fx;		//!< total forces on x axis
	double fy;		//!< total forces on y axis
	double fz;		//!< total forces on z axis

	double vx;		//!< velocity on x axis
	double vy;		//!< velocity on y axis
	double vz;		//!< velocity on z axis

	density_operator rho_p;	//!< density operator of the particle

	//! Particle constructor
	Particle(int nlevels = NLEVELS) :
		x(0.0), y(0.0), z(0.0), fx(0.0), fy(0.0), fz(0.0), vx(0.0), vy(0.0), vz(0.0) , rho_p(nlevels)
	{
		//_m = std::make_shared<std::mutex>();
	};

	//! Copy constructor
	Particle(const Particle &other) :
		x(other.x), y(other.y), z(other.z), fx(other.fx), fy(other.fy), fz(other.fz), vx(other.vx), vy(other.vy), vz(other.vz), rho_p(other.rho_p)//, _m(other._m)
	{};

	//! update the position, velocity and density operator of the particle - without EMFs
	double update(double time, double dt) {

		//free parameters for an external force 
		double fr_x_ext = 0;
		double fr_y_ext = 0;
		double fr_z_ext = 0;

		
		//external force you may want to add - maybe a trapping one
		double k = 0.;
		fr_x_ext += 0;
		fr_y_ext += 0;
		fr_z_ext += 0;

		//compute acceleration
		double ax = fx / ARmass + fr_x_ext;
		double ay = fy / ARmass + fr_y_ext;
		double az = fz / ARmass + fr_z_ext;

		//update velocity and position using verlet algorithm
		vx += ax * 0.5*dt;
		vy += ay * 0.5*dt;
		vz += az * 0.5*dt;
		x += dt * vx;
		y += dt * vy;
		z += dt * vz;
		fx = 0.0;
		fy = 0.0;
		fz = 0.0;

		//return the kinetic energy
		return 0.5*ARmass*(vx*vx + vy * vy + vz * vz);
	}

	//! update the position, velocity and density operator of the particle - with EMFs fields
	double update(double time,double dt, vector<ElectricField> EMFs) {
	
		//free parameters for an external force 
		double fr_x_ext = 0;
		double fr_y_ext = 0;
		double fr_z_ext = 0;

		// a RK4 integration technique for the quantum part shall appear here E_p.Value(x,y,z,time*DT)
		if (rho_p.levels != 0) 
		{
			rho_p.push_rk4(EMFs, x, y, z, time, dt, vx, vy, vz);

			//compute the effect of the dipolar force

			fr_x_ext += -10000.*EMFs[0].gradx(x, y, z, time*dt)*(2 * (rho_p.rhos_j[2].real()));
			fr_y_ext += -10000.*EMFs[0].grady(x, y, z, time*dt)*(2 * (rho_p.rhos_j[2]).real());
			fr_z_ext += -10000.*EMFs[0].gradz(x, y, z, time*dt)*(2 * (rho_p.rhos_j[2].real()));

			fr_x_ext += -10000.*EMFs[1].gradx(x, y, z, time*dt)*(2 * (rho_p.rhos_j[2].real()));
			fr_y_ext += -10000.*EMFs[1].grady(x, y, z, time*dt)*(2 * (rho_p.rhos_j[2]).real());
			fr_z_ext += -10000.*EMFs[1].gradz(x, y, z, time*dt)*(2 * (rho_p.rhos_j[2].real()));

		}

		//external force you may want to add - maybe a trapping one
		double k = 0.;
		fr_x_ext += 0;
		fr_y_ext += 0;
		fr_z_ext += 0;

		//compute acceleration
		double ax = fx / ARmass + fr_x_ext;
		double ay = fy / ARmass + fr_y_ext;
		double az = fz / ARmass + fr_z_ext;

		//update velocity and position using verlet algorithm
		vx += ax * 0.5*dt;
		vy += ay * 0.5*dt;
		vz += az * 0.5*dt;
		x += dt * vx;
		y += dt * vy;
		z += dt * vz;
		fx = 0.0;
		fy = 0.0;
		fz = 0.0;
		
		//return the kinetic energy
		return 1.;// 0.5*ARmass*(vx*vx + vy * vy + vz * vz);
	}

	//! Collective multiple scattering force
	double interact_MS(Particle &atom2, double time, ElectricField EMF)
	{
		double rx, ry, rz, r, fx_t, fy_t, fz_t, f_t;
		double sigmaL = 0.01;
		double sigmaR = 0.01;

		// computing base values
		rx = x - atom2.x;
		ry = y - atom2.y;
		rz = z - atom2.z;
		r = rx * rx + ry * ry + rz * rz;

		if (r < 0.0001)
		{
			return 1.0;
		}

		else
		{
			f_t = EMF.envelope(x, y, z, time) / r;
			fx_t = f_t * rx;
			fy_t = f_t * ry;
			fz_t = f_t * rz;

			// updating particle properties
			//std::lock_guard<std::mutex> l(*_m);

			fx += fx_t;
			fy += fy_t;
			fz += fz_t;
			return 1.;
		}
	}

	//! Collective multiple scattering force - overload for neighboor cells
	double interact_MS(double a2x, double a2y, double a2z, double time, ElectricField EMF)
	{
		double rx, ry, rz, r, fx_t, fy_t, fz_t, f_t;
		double sigmaL = 0.01;
		double sigmaR = 0.01;

		// computing base values
		rx = x - a2x;
		ry = y - a2y;
		rz = z - a2z;
		r = rx * rx + ry * ry + rz * rz;

		if (r < 0.0001)
		{
			return 1.0;
		}

		else {
			f_t = EMF.envelope(x, y, z, time) / r;
			fx_t = f_t * rx;
			fy_t = f_t * ry;
			fz_t = f_t * rz;

			// updating particle properties
			//std::lock_guard<std::mutex> l(*_m);

			fx += fx_t;
			fy += fy_t;
			fz += fz_t;
			return 1.;
		}
	}

	//! Force calculation of the LJ type
	double interact_LJ(Particle &atom2)
	{
		double rx, ry, rz, r, fx_t, fy_t, fz_t, f_t;
		double sigma6, sigma12;

		// computing base values
		rx = x - atom2.x;
		ry = y - atom2.y;
		rz = z - atom2.z;
		r = rx * rx + ry * ry + rz * rz;

		if (r < 0.000001)
			return 0.0;

		r = sqrt(r);
		sigma6 = pow((ARsigma / r), 6);
		sigma12 = sigma6 * sigma6;
		f_t = ((sigma12 - 0.5*sigma6)*48.0*AReps) / r;
		fx_t = f_t * rx;
		fy_t = f_t * ry;
		fz_t = f_t * rz;

		// updating particle properties
		fx += fx_t;
		fy += fy_t;
		return 4.0*AReps*(sigma12 - sigma6);
	}
};