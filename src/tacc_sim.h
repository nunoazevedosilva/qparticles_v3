#pragma once
#include "Cell_system_node.h"

double t_letter(double x, double y, double z, double t, double Lx, double Ly, double Lz)
{
	////////////////////////////////////////////////

	//Setting the boundary of the cell
	double x_0 = 0;
	double y_0 = 0;
	double z_0 = 0;

	double x_1 = Lx / 4.;
	double y_1 = Ly / 4.;
	double z_1 = Lz / 4.;

	double ww = x_1 / 5.;

	////////////////////////////////////////////////

	if (x > (x_1 / 8.) && x < (7 * x_1 / 8.) && y >(2 * Ly / 8.) && y < (2 * Ly / 8. + ww))
	{
		return 1;
	}
	else if (x > (x_1 / 2. - ww / 2.) && x < (x_1 / 2. + ww / 2.) && y >(2 * Ly / 8. + ww) && y < (7 * Ly / 8.))
	{
		return 1;
	}

	else
		return 0;


}

double c_letter(double x, double y, double z, double t, double offsetx, double Lx, double Ly, double Lz)
{
	////////////////////////////////////////////////



	//Setting the boundary of the cell
	double x_0 = 0 + offsetx;
	double y_0 = 0;
	double z_0 = 0;

	double x_1 = Lx / 4. + offsetx;
	double y_1 = Ly / 4.;
	double z_1 = Lz / 4.;

	double size = (x_1 - x_0);
	double ww = size / 5.;

	////////////////////////////////////////////////

	if (x > (x_0 + size / 8.) && x < (x_0 + 7 * size / 8.) && y >(2 * Ly / 8.) && y < (2 * Ly / 8. + ww))
	{
		return 1;
	}
	else if (x > (x_0 + size / 8.) && x < (x_0 + 7 * size / 8.) && y <(7 * Ly / 8.) && y >(7 * Ly / 8. - ww))
	{
		return 1;
	}

	else if (x > (x_0 + size / 8.) && x < (x_0 + size / 8. + ww) && y >(2 * Ly / 8. + ww) && y < (7 * Ly / 8.))
	{
		return 1;
	}

	else
		return 0;


}

double a_letter(double x, double y, double z, double t, double offsetx, double Lx, double Ly, double Lz)
{
	////////////////////////////////////////////////

	//Setting the boundary of the cell
	double x_0 = 0 + offsetx;
	double y_0 = 0;
	double z_0 = 0;

	double x_1 = Lx / 4. + offsetx;
	double y_1 = Ly / 4.;
	double z_1 = Lz / 4.;

	double size = (x_1 - x_0);
	double ww = size / 5.;

	////////////////////////////////////////////////

	if (x > (x_0 + size / 8.) && x < (x_0 + 7 * size / 8.) && y >(2 * Ly / 8.) && y < (2 * Ly / 8. + ww))
	{
		return 1;
	}
	else if (x > (x_0 + size / 8.) && x < (x_0 + 7 * size / 8.) && y <(5 * Ly / 8.) && y >(5 * Ly / 8. - ww))
	{
		return 1;
	}

	else if (x > (x_0 + size / 8.) && x < (x_0 + size / 8. + ww) && y >(2 * Ly / 8. + ww) && y < (7 * Ly / 8.))
	{
		return 1;
	}

	else if (x < (x_0 + 7 * size / 8.) && x >(x_0 + 7 * size / 8. - ww) && y > (2 * Ly / 8. + ww) && y < (7 * Ly / 8.))
	{
		return 1;
	}


	else
		return 0;


}

double tacc(double x, double y, double z, double t)
{
	////////////////////////////////////////////////

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 1.0*10e2;
	double Lz = 1.0*10e1;

	return (t_letter(x, y, z, t, Lx, Ly, Lz) + a_letter(x, y, z, t, Lx / 4., Lx, Ly, Lz) +
		c_letter(x, y, z, t, 2 * Lx / 4., Lx, Ly, Lz) + c_letter(x, y, z, t, 3 * Lx / 4., Lx, Ly, Lz))*exp(-(t - 20)*(t - 20) / 15.);

}





int tacc_sim(int argc, char* argv[])
{

	int n_threads = 2;

	//integration step
	double dt = 0.05;
	//Number of iterations
	int NumIterations = 8000;

	//density of the gas
	const double density = 10e-5; //parts/micrometer**3

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 1.0*10e2;
	double Lz = 2.0*10e1;

	//set a mesh for saving the EMF field
	int Nx = 100;
	int Ny = 100;
	int Nz = 10;

	//create a mesh with simulation parameters
	mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

	//////////////////////////////////////////////////
	//initialize the need electric fields
	ElectricField E_t = ElectricField(0.1, -0.01, "E_p1", tacc);
	ElectricField E_a = ElectricField(0.5, -0.01, "E_p2", tacc);

	vector<ElectricField> EMFs;
	EMFs.push_back(E_t);
	EMFs.push_back(E_a);

	///////////////////////////////////////////////

	//parameters for the simulation - machine related
	int TotalNodes_X = 1;
	int TotalNodes_Y = 1;
	int TotalNodes_Z = 1;

	int Ncells_X_node = int(40 / TotalNodes_X);
	int Ncells_Y_node = int(10 / TotalNodes_Y);
	int Ncells_Z_node = 5;

	int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node*TotalNodes_X*TotalNodes_Y*TotalNodes_Z;

	//dimensions of the cell in micrometers
	double Cell_lx = Lx / TotalNodes_X / Ncells_X_node;
	double Cell_ly = Ly / TotalNodes_Y / Ncells_Y_node;
	double Cell_lz = Lz / TotalNodes_Z / Ncells_Z_node;

	//number of particles
	const int NPartPerCell = int(density*Cell_lx*Cell_ly*Cell_lz);
	int NumParticles = density * Lx*Ly*Lz;

	//create the cell system in 1 node
	Cell_system_node total_cells = Cell_system_node(TotalNodes_X, TotalNodes_Y, TotalNodes_Z, Ncells_X_node, Ncells_Y_node, Ncells_Z_node, Cell_lx, Cell_ly, Cell_lz, dt, my_mesh, EMFs, NPartPerCell, NLEVELS, true, n_threads);

	cout << "\nThe Total Number of Cells is " << TotalCells
		<< "\nand " << NumParticles << " particles total in system"
		<< "\nand " << NPartPerCell << " particles in each cell \n";

	cout << "Cells done\n";

	//start a timer
	auto start = std::chrono::system_clock::now();

	//run the simulation

	total_cells.run(NumIterations, 10, dt, "tacc_sim_4//", "short_range");


	//end the timer
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	cout << endl << "time taken = " << elapsed.count() << "ms" << '\n';

	//int a = getchar();
	return 0;
}


int tacc_sim2(int argc, char* argv[])
{

	int n_threads = 2;

	//integration step
	double dt = 0.05;
	//Number of iterations
	int NumIterations = 3;

	//density of the gas
	const double density = 0.5*10e-4; //parts/micrometer**3

	//Dimension of the total system in micrometers
	double Lx = 2.0*10e2;
	double Ly = 1.0*10e2;
	double Lz = 2.0*10e1;

	//set a mesh for saving the EMF field
	int Nx = 100;
	int Ny = 100;
	int Nz = 10;

	//create a mesh with simulation parameters
	mesh my_mesh = mesh(Nx, Ny, Nz, Lx, Ly, Lz);

	//////////////////////////////////////////////////
	//initialize the need electric fields
	ElectricField E_t = ElectricField(0.1, -0.01, "E_p1", tacc);
	ElectricField E_a = ElectricField(0.5, -0.01, "E_p2", tacc);

	vector<ElectricField> EMFs;
	EMFs.push_back(E_t);
	EMFs.push_back(E_a);

	///////////////////////////////////////////////

	//parameters for the simulation - machine related
	int TotalNodes_X = 1;
	int TotalNodes_Y = 1;
	int TotalNodes_Z = 1;

	int Ncells_X_node = int(40 / TotalNodes_X);
	int Ncells_Y_node = int(10 / TotalNodes_Y);
	int Ncells_Z_node = 5;

	int TotalCells = Ncells_X_node * Ncells_Y_node * Ncells_Z_node*TotalNodes_X*TotalNodes_Y*TotalNodes_Z;

	//dimensions of the cell in micrometers
	double Cell_lx = Lx / TotalNodes_X / Ncells_X_node;
	double Cell_ly = Ly / TotalNodes_Y / Ncells_Y_node;
	double Cell_lz = Lz / TotalNodes_Z / Ncells_Z_node;

	//number of particles
	const int NPartPerCell = int(density*Cell_lx*Cell_ly*Cell_lz);
	int NumParticles = density * Lx*Ly*Lz;

	//create the cell system in 1 node
	Cell_system_node total_cells = Cell_system_node(TotalNodes_X, TotalNodes_Y, TotalNodes_Z, Ncells_X_node, Ncells_Y_node, Ncells_Z_node, Cell_lx, Cell_ly, Cell_lz, dt, my_mesh, EMFs, NPartPerCell, NLEVELS, true, n_threads);

	cout << "\nThe Total Number of Cells is " << TotalCells
		<< "\nand " << NumParticles << " particles total in system"
		<< "\nand " << NPartPerCell << " particles in each cell \n";

	cout << "Cells done\n";

	//start a timer
	auto start = std::chrono::system_clock::now();

	//run the simulation

	total_cells.run(NumIterations, 10, dt, "tacc_sim_4//", "short_range");


	//end the timer
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	cout << endl << "time taken = " << elapsed.count() << "ms" << '\n';

	//int a = getchar();
	return 0;
}



